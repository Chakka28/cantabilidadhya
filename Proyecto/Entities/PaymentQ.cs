﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Entities
{
    public class PaymentQ
    {
        public int id { get; set; }
        public int cliente_id { get; set; }
        public string nombre_cliente { get; set; }
        public string fecha { get; set; }
        public string factura_electronica { get; set; }
        public string monto { get; set; }
        public string estado { get; set; }
        public string tipo { get; set; }
        public string ano { get; set; }
        public string nota { get; set; }
        public int active { get; set; }
    }
}
