﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Entities
{
    public class Parnet
    {
        public int id { get; set; }
        public string nombre_socio { get; set; }
        public string nombre_comercial { get; set; }
        public string cedula { get; set; }
        public string correo { get; set; }
        public string direccion{ get; set; }
        public string clave_correo { get; set; }
        public string nacimiento { get; set; }
        public string celular { get; set; }
        public string nise { get; set; }
        public string ven { get; set; }
        public int active { get; set; }
    }
}
