﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.Clients
{
    public partial class FormClient : Form
    {
        private ParnetBOL parnetBOL;//Clase bol de socio
        private List<Parnet> parnets = new List<Parnet>();//lista de socios
        public Client client = new Client(); //objeto de cliente
        private ClientBOL clientBOL; //clase bol de Cliente
        private string type_funcion; // Variable que define como se usara el formulario editar, agregar o ver detalle

        public FormClient(string type, Client clientEnter)
        {
            InitializeComponent();
            client = clientEnter;
            type_funcion = type;
        }

        private void FormClient_Load(object sender, EventArgs e)
        {
            parnetBOL = new ParnetBOL();
            clientBOL = new ClientBOL();
            Load_Parnet();
            if (type_funcion == "edit")
            {
                edit();
            }
            if (type_funcion == "detail")
            {
                detail();
            }
        }

        /// <summary>
        /// Carga la lista de socios en el combo box
        /// </summary>
        public void Load_Parnet()
        {
            parnets = parnetBOL.Load_parnets("");
            cblistasocios.DataSource = parnets;
            Load_dato_Parnet();
        }

        /// <summary>
        /// Carga los datos de los socios
        /// </summary>
        public void Load_dato_Parnet()
        {
            if (clientBOL.exists(client.id_socio.ToString()) || client.id_socio == -1)
            {
                foreach (Parnet item in parnets)
                {
                    if (item.id.ToString() == cblistasocios.SelectedValue.ToString())
                    {
                        txtSCedula.Text = item.cedula;
                        txtSnombre_comercial.Text = item.nombre_comercial;
                        txtSnombre_comercial.Enabled = false;
                        txtSCedula.Enabled = false;
                    }
                }
                
            }
            else
            {
                txtSnombre_comercial.Enabled = false;
                txtSCedula.Enabled = false;
                lblmensaje.Visible = true;
                lblmensaje.Text = "ESTE SOCIO FUE ELIMINADO, VE A EDITAR PARA AGREGARLO A ESTE CLIENTE A UN NUEVO SOCIO";
            }
        }

        /// <summary>
        /// Rellenar los campos para editar
        /// </summary>
        public void edit()
        {
            cblistasocios.SelectedValue = client.id_socio;
            txtnombre_comercial.Text = client.nombre_comercial;
            txtNombreSocio.Text = client.nombre_cliente;
            txtCedula.Text = client.cedula;
            txtCelular.Text = client.celular;
            txtCorreo.Text = client.correo;
            txtClave.Text = client.clave_correo;
            DateTime enteredDate = DateTime.Parse(client.nacimiento);
            txtNac.Value = enteredDate;
            DateTime enteredDate2 = DateTime.Parse(client.ven);
            txtVen.Value = enteredDate2;
            txtNice.Text = client.nise;
            txtDireccion.Text = client.direccion;
        }


        /// <summary>
        /// Rellenar los campos para los detalles
        /// </summary>
        public void detail()
        {
            cblistasocios.SelectedValue = client.id_socio;
            cblistasocios.Enabled = false;
            txtnombre_comercial.Text = client.nombre_comercial;
            txtnombre_comercial.Enabled = false;
            txtNombreSocio.Text = client.nombre_cliente;
            txtNombreSocio.Enabled = false;
            txtCedula.Text = client.cedula;
            txtCedula.Enabled = false;
            txtCelular.Text = client.celular;
            txtCelular.Enabled = false;
            txtCorreo.Text = client.correo;
            txtCorreo.Enabled = false;
            txtClave.Text = client.clave_correo;
            txtClave.Enabled = false;
            DateTime enteredDate = DateTime.Parse(client.nacimiento);
            txtNac.Value = enteredDate;
            txtNac.Enabled = false;
            DateTime enteredDate2 = DateTime.Parse(client.ven);
            txtVen.Value = enteredDate2;
            txtVen.Enabled = false;
            txtNice.Text = client.nise;
            txtNice.Enabled = false;
            txtDireccion.Text = client.direccion;
            txtDireccion.Enabled = false;
            btnGuardar.Enabled = false;
        }

        /// <summary>
        /// Boton para salir del formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVolver_Click_1(object sender, EventArgs e)
        {
            Form form = new Client_index();
            form.Show();
            this.Close();
        }

        /// <summary>
        /// Evento al hacer click al combo box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cblistasocios_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_dato_Parnet();
        }

        /// <summary>
        /// Revisa los datos nulos
        /// </summary>
        /// <returns></returns>
        public bool chack_datos()
        {
            if (txtnombre_comercial.Text == "" && txtNombreSocio.Text == ""
                && txtCedula.Text == "" && txtCelular.Text == ""
                && txtCorreo.Text == "" && txtClave.Text == ""
                && txtNice.Text == "" && txtDireccion.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Revisa los datos a la hora de editar
        /// </summary>
        /// <returns></returns>
        public bool Check_edit()
        {
            string dateNac = txtNac.Value.ToString("dd/MM/yyyy");
            string dateVen = txtVen.Value.ToString("dd/MM/yyyy");

            if (txtnombre_comercial.Text == client.nombre_comercial && txtNombreSocio.Text == client.nombre_cliente
                && txtCedula.Text == client.cedula && txtCelular.Text == client.celular
                && txtCorreo.Text == client.correo && txtClave.Text == client.clave_correo
                && txtNice.Text == client.nise && txtDireccion.Text == client.direccion
                && dateNac == client.nacimiento && dateVen == client.ven
                && client.id.ToString() == cblistasocios.SelectedValue.ToString())
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Carga el objeto cliente y lo gguarda en la base de datos
        /// </summary>
        private void Save_Parnet()
        {
            //Converter the date from datetime to string 
            string dateNac = txtNac.Value.ToString("dd/MM/yyyy");
            string dateVen = txtVen.Value.ToString("dd/MM/yyyy");
            //Parnet object with data
            Client item = new Client();
            item.id_socio = Int32.Parse(cblistasocios.SelectedValue.ToString()); ;
            item.nombre_cliente = txtNombreSocio.Text;
            item.nombre_comercial = txtnombre_comercial.Text;
            item.cedula = txtCedula.Text;
            item.correo = txtCorreo.Text;
            item.clave_correo = txtClave.Text;
            item.direccion = txtDireccion.Text;
            item.celular = txtCelular.Text;
            item.active = 1;
            item.nacimiento = dateNac;
            item.nise = txtNice.Text;
            item.ven = dateVen;
            if (type_funcion == "edit")
            {
                try
                {
                    item.id = client.id;
                    clientBOL.Modify_parent(item);
                    MessageBox.Show("El socio de a guardado con exito");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

            }
            else
            {
                try
                {
                    clientBOL.Save(item);
                    MessageBox.Show("El socio de a guardado con exito");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        // exit the this form
        private void exit_form()
        {
            Form form = new Client_index();
            form.Show();
            this.Close();
        }


        /// <summary>
        /// Valida y carga el cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (parnets != null)
            {
                if (type_funcion == "edit")
                {
                    if (chack_datos())
                    {
                        MessageBox.Show("Todos los datos estan vacios");
                    }
                    else
                    {
                        if (Check_edit())
                        {
                            MessageBox.Show("No has realizado ningun cambio");
                        }
                        else
                        {
                            //Message
                            const string message = "Deseas realizar los cambios?";
                            const string caption = "Aviso";
                            var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    Save_Parnet();
                                    exit_form();
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (chack_datos())
                    {
                        MessageBox.Show("Todos los datos estan vacios");
                    }
                    else
                    {
                        //check the empty data
                        if (txtnombre_comercial.Text == "" || txtNombreSocio.Text == ""
                        || txtCedula.Text == "" || txtCelular.Text == ""
                        || txtCorreo.Text == "" || txtClave.Text == ""
                        || txtNice.Text == "" || txtDireccion.Text == "")
                        {
                            //Message
                            const string message = "Deseas guardar este socios con algunos datos vacios?";
                            const string caption = "Datos Vacios";
                            var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    Save_Parnet();
                                    exit_form();
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                Save_Parnet();
                                exit_form();
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No hay socios para agregar cliente, necesitas agregar socio");
            }
            
        }

        /// <summary>
        /// Asegura que en el txt de celular solo se pueda usar numeros
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
    }
}
