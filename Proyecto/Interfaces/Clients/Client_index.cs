﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.Clients
{
    public partial class Client_index : Form
    {
        private ClientBOL clientBOL;//Clase de cliente bol

        public Client_index()
        {
            InitializeComponent();
        }

        private void Cliente_Load(object sender, EventArgs e)
        {
            clientBOL = new ClientBOL();
            Load_DataList();
        }

        /// <summary>
        /// Carga el datagridview de clientes
        /// </summary>
        public void Load_DataList()
        {
            DGclientes.DataSource = clientBOL.Load_clients("");
        }

        /// <summary>
        /// Boton para volver al form principal
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVolver_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Realiza verificaciones y llama al formclient
        /// </summary>
        /// <param name="type">variable que decide la funcion del formclient</param>
        private void Form_client(string type)
        {
            if (type != "")
            {
                int row = DGclientes.SelectedRows.Count > 0 ? DGclientes.SelectedRows[0].Index : -1;
                if (row >= 0)
                {
                    Form formParnet = new FormClient(type, (Client)DGclientes.CurrentRow.DataBoundItem);
                    formParnet.Show();
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Seleccione un socio de la tabla");
                }
            }
            else
            {
                Client mp = new Client();
                mp.id_socio = -1;
                Form formParnet = new FormClient(type, mp);
                formParnet.Show();
                this.Close();
            }
        }
        /// <summary>
        /// Boton de agregar cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddClient_Click_1(object sender, EventArgs e)
        {
            Form_client("");
        }
        /// <summary>
        /// Boton de detalle de cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetalle_Click(object sender, EventArgs e)
        {
            Form_client("detail");
        }

        /// <summary>
        /// Boton de editar cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            Form_client("edit");
        }

        /// <summary>
        /// Boton de eliminar cliente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Client cl = new Client();
            int row = DGclientes.SelectedRows.Count > 0 ? DGclientes.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                cl = (Client)DGclientes.CurrentRow.DataBoundItem;
                const string message = "Deseas eliminar a este cliente?\n" +
                    "En el caso de que este cliente tenga pagos agregados tambien se eliminarán";
                const string caption = "Eliminar cliente";
                var result = MessageBox.Show(message, caption,
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    clientBOL.Delete_client((Client)DGclientes.CurrentRow.DataBoundItem);
                    Load_DataList();
                }
            }
            else
            {
                MessageBox.Show("Seleccione un cliente de la tabla");
            }
        }

        /// <summary>
        /// Boton de cargar busqueda de clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            DGclientes.DataSource = clientBOL.Load_clients(txtbuscar.Text);
        }
    }
}
