﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.Parnets
{
    public partial class Parnet_index : Form
    {
        private ParnetBOL parnetBOL;//Clase BOL de socio

        public Parnet_index()
        {
            InitializeComponent();
        }

        private void Parnet_index_Load(object sender, EventArgs e)
        {
            parnetBOL = new ParnetBOL();//Se inicializa la clase bol
            Load_DataList();// Se cargan los socios
        }

        /// <summary>
        /// Carga los socios en el datagridview
        /// Manda llamar al metodo Load_parnets del bol que retorna una lista de socios
        /// </summary>
        public void Load_DataList()
        {
            DGSocios.DataSource = parnetBOL.Load_parnets("");
        }

        /// <summary>
        /// Llama al formulario para agregar un nuevo socio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddSocio_Click(object sender, EventArgs e)
        {
            Form_parnet("");
        }

        /// <summary>
        /// Llama al formulario para editar un socio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            Form_parnet("edit");
        }

        /// <summary>
        /// Llama al formulario para ver los detalles de un socio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetalle_Click(object sender, EventArgs e)
        {
            Form_parnet("detail");
        }

        /// <summary>
        /// Cierra este formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Realiza una busqueda de socios con filtro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DGSocios.DataSource = parnetBOL.Load_parnets(txtBuscar.Text);
        }

        
        /// <summary>
        /// Metodo que ejecuta las validaciones y llama al formulario
        /// </summary>
        /// <param name="type">Variable que indica para que se utilizara el formulario</param>
        private void Form_parnet(string type)
        {
            if (type != "")
            {
                int row = DGSocios.SelectedRows.Count > 0 ? DGSocios.SelectedRows[0].Index : -1;
                if (row >= 0)
                {
                    Form formParnet = new FormParnet(type, (Parnet)DGSocios.CurrentRow.DataBoundItem);
                    formParnet.Show();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Seleccione un socio de la tabla");
                }
            }
            else
            {
                Parnet mp = new Parnet();
                Form formParnet = new FormParnet(type, mp);
                formParnet.Show();
                this.Close();
            }
        }

        /// <summary>
        /// Elimina un socio
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            int row = DGSocios.SelectedRows.Count > 0 ? DGSocios.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                const string message = "Deseas eliminar este socio?";
                const string caption = "Eliminar socio";
                var result = MessageBox.Show(message, caption,
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    parnetBOL.Delete_parent((Parnet)DGSocios.CurrentRow.DataBoundItem);
                    Load_DataList();
                }
            }
            else
            {
                MessageBox.Show("Seleccione un socio de la tabla");
            }
        }
    }
}
