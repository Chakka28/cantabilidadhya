﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.Parnets
{
    public partial class FormParnet : Form
    {
        public Parnet Parnet = new Parnet(); //Objeto de socio
        private ParnetBOL parnetBOL; //clase de BOL de socio
        private string type_funcion; // String que indica la funcion de este form: agregar, detalle o editar

        public FormParnet(string type, Parnet ParnetEnter)
        {
            InitializeComponent();
            Parnet = ParnetEnter;
            type_funcion = type;
            if (type == "edit")
            {
                editar();
            }
            if (type == "detail")
            {
                detalle();
            }
        }


        /// <summary>
        /// En el caso de detalle este metodo llena los campos y inabilita el poder editar estos compos ya que solo se 
        /// pueden visualizar
        /// </summary>
        public void detalle()
        {
            txtnombre_comercial.Text = Parnet.nombre_comercial;
            txtnombre_comercial.Enabled = false;
            txtNombreSocio.Text = Parnet.nombre_socio;
            txtNombreSocio.Enabled = false;
            txtCedula.Text = Parnet.cedula;
            txtCedula.Enabled = false;
            txtCelular.Text = Parnet.celular;
            txtCelular.Enabled = false;
            txtCorreo.Text = Parnet.correo;
            txtCorreo.Enabled = false;
            txtClave.Text = Parnet.clave_correo;
            txtClave.Enabled = false;
            DateTime enteredDate = DateTime.Parse(Parnet.nacimiento);
            txtNac.Value = enteredDate;
            txtNac.Enabled = false;
            DateTime enteredDate2 = DateTime.Parse(Parnet.ven);
            txtVen.Value = enteredDate2;
            txtVen.Enabled = false;
            txtNice.Text = Parnet.nise;
            txtNice.Enabled = false;
            txtDireccion.Text = Parnet.direccion;
            txtDireccion.Enabled = false;
            btnGuardar.Enabled = false;
        }

        /// <summary>
        /// En el caso de editar este metodo rellena los campos respectivos
        /// </summary>
        public void editar()
        {
            txtnombre_comercial.Text = Parnet.nombre_comercial;
            txtNombreSocio.Text = Parnet.nombre_socio;
            txtCedula.Text = Parnet.cedula;
            txtCelular.Text = Parnet.celular;
            txtCorreo.Text = Parnet.correo;
            txtClave.Text = Parnet.clave_correo;
            DateTime enteredDate = DateTime.Parse(Parnet.nacimiento);
            txtNac.Value = enteredDate;
            DateTime enteredDate2 = DateTime.Parse(Parnet.ven);
            txtVen.Value = enteredDate2;
            txtNice.Text = Parnet.nise;
            txtDireccion.Text = Parnet.direccion;
        }

        /// <summary>
        /// Cierra este formulario
        /// </summary>
        private void exit_form()
        {
            Form formParnet = new Parnet_index();
            formParnet.Show();
            this.Close();
        }

        /// <summary>
        /// Boton de guardar
        /// type_funcion: verifica que tipo de funcion de va a ser si editar o agregar uno nuevo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(type_funcion == "edit")
            {
                if (chack_datos())
                {
                    MessageBox.Show("Todos los datos estan vacios");
                }
                else
                {
                    if (Check_edit())
                    {
                        MessageBox.Show("No has realizado ningun cambio");
                    }
                    else
                    {
                        //Message
                        const string message = "Deseas realizar los cambios?";
                        const string caption = "Aviso";
                        var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            try
                            {
                                Save_Parnet();
                                exit_form();
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
             }
            else
            {
                if (chack_datos())
                {
                    MessageBox.Show("Todos los datos estan vacios");
                }
                else
                {
                    //check the empty data
                    if (txtnombre_comercial.Text == "" || txtNombreSocio.Text == ""
                    || txtCedula.Text == "" || txtCelular.Text == ""
                    || txtCorreo.Text == "" || txtClave.Text == ""
                    || txtNice.Text == "" || txtDireccion.Text == "")
                    {
                        //Message
                        const string message = "Deseas guardar este socios con algunos datos vacios?";
                        const string caption = "Datos Vacios";
                        var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            try
                            {
                                Save_Parnet();
                                exit_form();
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        try
                        {
                            Save_Parnet();
                            exit_form();
                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show(ex.Message);
                        }
                    }
                }
            }
                
        }

        /// <summary>
        /// Crea el objeto socio con los datos correspondientes y manda a llamar 
        /// a su respectiva funcion ya sea un nuevo socio o editar un socio
        /// </summary>
        private void Save_Parnet()
        {
            //de datatime a string
            string dateNac = txtNac.Value.ToString("dd/MM/yyyy");
            string dateVen = txtVen.Value.ToString("dd/MM/yyyy");
            //Socio
            Parnet parnet = new Parnet();
            parnet.nombre_socio = txtNombreSocio.Text;
            parnet.nombre_comercial = txtnombre_comercial.Text;
            parnet.cedula = txtCedula.Text;
            parnet.correo = txtCorreo.Text;
            parnet.clave_correo = txtClave.Text;
            parnet.direccion = txtDireccion.Text;
            parnet.celular = txtCelular.Text;
            parnet.active = 1;
            parnet.nacimiento = dateNac;
            parnet.nise = txtNice.Text;
            parnet.ven = dateVen;
            if(type_funcion == "edit")
            {
                try
                {
                    parnet.id = Parnet.id;
                    parnetBOL.Modify_parent(parnet);
                    MessageBox.Show("El socio de a guardado con exito");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                
            }
            else
            {
                try
                {
                    parnetBOL.Save(parnet);
                    MessageBox.Show("El socio de a guardado con exito");
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            }
            

        }

        /// <summary>
        /// Revisa si todos los datos estan nulos
        /// </summary>
        /// <returns>bool: true si estan nulos, false si no</returns>
        public bool chack_datos()
        {
            if (txtnombre_comercial.Text == "" && txtNombreSocio.Text == ""
                && txtCedula.Text == "" && txtCelular.Text == ""
                && txtCorreo.Text == "" && txtClave.Text == ""
                && txtNice.Text == "" && txtDireccion.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Revisa si a la hora de guardar un usuario editado no 
        /// esten los mismos datos
        /// </summary>
        /// <returns>bool: true si estan nulos, false si no</returns>
        public bool Check_edit()
        {
            string dateNac = txtNac.Value.ToString("dd/MM/yyyy");
            string dateVen = txtVen.Value.ToString("dd/MM/yyyy");

            if (txtnombre_comercial.Text == Parnet.nombre_comercial && txtNombreSocio.Text == Parnet.nombre_socio
                && txtCedula.Text == Parnet.cedula && txtCelular.Text == Parnet.celular
                && txtCorreo.Text == Parnet.correo && txtClave.Text == Parnet.clave_correo
                && txtNice.Text == Parnet.nise && txtDireccion.Text == Parnet.direccion
                && dateNac == Parnet.nacimiento && dateVen == Parnet.ven)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Boton para volver a a index de socios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVolver_Click(object sender, EventArgs e)
        {
            exit_form();
        }
       
        private void FormParnet_Load(object sender, EventArgs e)
        {
            parnetBOL = new ParnetBOL();
        }

        /// <summary>
        /// Hacer que el textbox  de celular solo acepte numeros 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
    }
}
