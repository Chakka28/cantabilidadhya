﻿namespace Proyecto.Interfaces
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRespaldo = new System.Windows.Forms.Button();
            this.btnOtros_Pagos = new System.Windows.Forms.Button();
            this.btnPagos = new System.Windows.Forms.Button();
            this.BtnSocio = new System.Windows.Forms.Button();
            this.BtnClient = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Tan;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnRespaldo);
            this.panel1.Controls.Add(this.btnOtros_Pagos);
            this.panel1.Controls.Add(this.btnPagos);
            this.panel1.Controls.Add(this.BtnSocio);
            this.panel1.Controls.Add(this.BtnClient);
            this.panel1.Controls.Add(this.btnExit);
            this.panel1.Location = new System.Drawing.Point(16, 64);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(810, 167);
            this.panel1.TabIndex = 3;
            // 
            // btnRespaldo
            // 
            this.btnRespaldo.BackColor = System.Drawing.Color.Wheat;
            this.btnRespaldo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRespaldo.BackgroundImage")));
            this.btnRespaldo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRespaldo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRespaldo.Location = new System.Drawing.Point(4, 121);
            this.btnRespaldo.Margin = new System.Windows.Forms.Padding(4);
            this.btnRespaldo.Name = "btnRespaldo";
            this.btnRespaldo.Size = new System.Drawing.Size(300, 38);
            this.btnRespaldo.TabIndex = 7;
            this.btnRespaldo.Text = "   Respaldo Base de Datos";
            this.btnRespaldo.UseVisualStyleBackColor = false;
            this.btnRespaldo.Click += new System.EventHandler(this.btnRespaldo_Click);
            // 
            // btnOtros_Pagos
            // 
            this.btnOtros_Pagos.BackColor = System.Drawing.Color.Wheat;
            this.btnOtros_Pagos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOtros_Pagos.BackgroundImage")));
            this.btnOtros_Pagos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnOtros_Pagos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOtros_Pagos.Location = new System.Drawing.Point(610, 15);
            this.btnOtros_Pagos.Margin = new System.Windows.Forms.Padding(4);
            this.btnOtros_Pagos.Name = "btnOtros_Pagos";
            this.btnOtros_Pagos.Size = new System.Drawing.Size(194, 92);
            this.btnOtros_Pagos.TabIndex = 6;
            this.btnOtros_Pagos.Text = "         Parciales";
            this.btnOtros_Pagos.UseVisualStyleBackColor = false;
            this.btnOtros_Pagos.Click += new System.EventHandler(this.btnOtros_Pagos_Click);
            // 
            // btnPagos
            // 
            this.btnPagos.BackColor = System.Drawing.Color.Wheat;
            this.btnPagos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPagos.BackgroundImage")));
            this.btnPagos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPagos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagos.Location = new System.Drawing.Point(408, 15);
            this.btnPagos.Margin = new System.Windows.Forms.Padding(4);
            this.btnPagos.Name = "btnPagos";
            this.btnPagos.Size = new System.Drawing.Size(194, 92);
            this.btnPagos.TabIndex = 5;
            this.btnPagos.Text = "         Pagos";
            this.btnPagos.UseVisualStyleBackColor = false;
            this.btnPagos.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // BtnSocio
            // 
            this.BtnSocio.BackColor = System.Drawing.Color.Wheat;
            this.BtnSocio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnSocio.BackgroundImage")));
            this.BtnSocio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnSocio.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSocio.Location = new System.Drawing.Point(4, 15);
            this.BtnSocio.Margin = new System.Windows.Forms.Padding(4);
            this.BtnSocio.Name = "BtnSocio";
            this.BtnSocio.Size = new System.Drawing.Size(194, 92);
            this.BtnSocio.TabIndex = 4;
            this.BtnSocio.Text = "        Socios";
            this.BtnSocio.UseVisualStyleBackColor = false;
            this.BtnSocio.Click += new System.EventHandler(this.BtnSocio_Click);
            // 
            // BtnClient
            // 
            this.BtnClient.BackColor = System.Drawing.Color.Wheat;
            this.BtnClient.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtnClient.BackgroundImage")));
            this.BtnClient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnClient.Location = new System.Drawing.Point(206, 15);
            this.BtnClient.Margin = new System.Windows.Forms.Padding(4);
            this.BtnClient.Name = "BtnClient";
            this.BtnClient.Size = new System.Drawing.Size(194, 92);
            this.BtnClient.TabIndex = 3;
            this.BtnClient.Text = "           Clientes";
            this.BtnClient.UseVisualStyleBackColor = false;
            this.BtnClient.Click += new System.EventHandler(this.BtnClient_Click_1);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.Wheat;
            this.btnExit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExit.BackgroundImage")));
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(680, 121);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(124, 38);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "   Cerrar";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.button3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Yellow;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(16, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(799, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Franklin Gothic Medium", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(319, 245);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(276, 30);
            this.label1.TabIndex = 5;
            this.label1.Text = "CONTROL Y CONFIANZA";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(287, 235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 27);
            this.label2.TabIndex = 6;
            this.label2.Text = "∞";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Yellow;
            this.ClientSize = new System.Drawing.Size(839, 274);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button BtnClient;
        private System.Windows.Forms.Button btnPagos;
        private System.Windows.Forms.Button BtnSocio;
        private System.Windows.Forms.Button btnOtros_Pagos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRespaldo;
    }
}