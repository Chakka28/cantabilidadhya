﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.PaymentQuarterly
{
    public partial class Payment_indexQ : Form
    {
        PaymentBOL payBOL;
        private List<Client> clients = new List<Client>();//list of the parents
        private ClientBOL clienteBOL;

        public Payment_indexQ()
        {
            InitializeComponent();
        }

        private void Payment_indexQ_Load(object sender, EventArgs e)
        {
            clienteBOL = new ClientBOL();
            payBOL = new PaymentBOL();
            Load_DataList();
            Load_client();
            dtano.Format = DateTimePickerFormat.Custom;
            dtano.CustomFormat = "yyyy";
            dtano.ShowUpDown = true;
        }

        public void Load_client()
        {
            Client cl = new Client() { id = 0, nombre_cliente = "" };
            clients = clienteBOL.Load_clients("");
            clients.Insert(0, cl);
            cbClient.DataSource = clients;
        }

        public void Load_DataList()
        {
            DGPagos.DataSource = payBOL.Load_Payment_parcial("", "Parcial");
        }


        private void Form_pay(string type)
        {
            if (type != "")
            {
                int row = DGPagos.SelectedRows.Count > 0 ? DGPagos.SelectedRows[0].Index : -1;
                if (row >= 0)
                {
                    Form formParnet = new  PaymentFormQ(type, (Payment)DGPagos.CurrentRow.DataBoundItem);
                    formParnet.Show();
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Seleccione un socio de la tabla");
                }
            }
            else
            {
                Payment mp = new Payment();
                Form formParnet = new PaymentFormQ(type, mp);
                formParnet.Show();
                this.Close();
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form_pay("");
        }

        private void btnDetalle_Click(object sender, EventArgs e)
        {
            Form_pay("detail");
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Form_pay("edit");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int row = DGPagos.SelectedRows.Count > 0 ? DGPagos.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                const string message = "Deseas eliminar este parcial?";
                const string caption = "Eliminar pago";
                var result = MessageBox.Show(message, caption,
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    payBOL.Delete_client((Payment)DGPagos.CurrentRow.DataBoundItem);
                    Load_DataList();
                }
            }
            else
            {
                MessageBox.Show("Seleccione un pago de la tabla");
            }
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filtro1 = "";
            string filtro2 = "";
            string filtro3 = "";
            string filtro4 = "";
            if (dtfecha.Checked == true)
            {
                filtro1 = dtfecha.Value.ToString("d/MM/yyyy");
            }
            if (cbClient.SelectedIndex != 0)
            {
                filtro2 = cbClient.SelectedValue.ToString();
            }
            if (cbmes.SelectedIndex != -1)
            {
                filtro3 = cbmes.SelectedItem.ToString();
            }
            if (dtano.Checked == true)
            {
                filtro4 = dtano.Value.Year.ToString();
            }

            DGPagos.DataSource = payBOL.Load_Payment_filter_parcial(filtro1, filtro2, filtro3, filtro4);
        }
    }
}
