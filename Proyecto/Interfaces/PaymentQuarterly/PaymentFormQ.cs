﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.PaymentQuarterly
{
    public partial class PaymentFormQ : Form
    {
        private ClientBOL clienteBOL;
        private string type_funcion; // String that means type of the funcion the this form
        private List<Client> clients = new List<Client>();//list of the parents
        public Payment pay = new Payment(); //Parent Obbject
        private PaymentBOL PayBOL;

        public PaymentFormQ(string type, Payment payItem)
        {
            InitializeComponent();
            type_funcion = type;
            pay = payItem;
        }

        private void PaymentFormQ_Load(object sender, EventArgs e)
        {
            clienteBOL = new ClientBOL();
            PayBOL = new PaymentBOL();
            envent_mains();
            Load_client();
            if (type_funcion == "edit")
            {
                edit();
            }
            if (type_funcion == "detail")
            {
                detail();
            }
        }

        public void detail()
        {
            cblistaclientes.SelectedValue = pay.cliente_id;
            cblistaclientes.Enabled = false;
            cbmes.SelectedItem = pay.mes;
            cbmes.Enabled = false;
            DateTime enteredDate = DateTime.Parse("01/01/" + pay.ano);
            dtano.Value = enteredDate;
            dtano.Enabled = false;
            txtMonto.Text = pay.monto;
            txtMonto.Enabled = false;
            rtNota.Text = pay.nota;
            rtNota.Enabled = false;
            btnGuardar.Enabled = false;
        }

        public void edit()
        {
            cblistaclientes.SelectedValue = pay.cliente_id;
            cbmes.SelectedItem = pay.mes;
            DateTime enteredDate = DateTime.Parse("01/01/" + pay.ano);
            dtano.Value = enteredDate;
            txtMonto.Text = pay.monto;
            rtNota.Text = pay.nota;
        }

        public void envent_mains()
        {
            dtano.Format = DateTimePickerFormat.Custom;
            dtano.CustomFormat = "yyyy";
            dtano.ShowUpDown = true;
            cbmes.SelectedIndex = 0;
        }

        public void Load_client()
        {
            clients = clienteBOL.Load_clients("");
            cblistaclientes.DataSource = clients;
            Load_dato_client();
        }

        // load the client informations 
        public void Load_dato_client()
        {
            foreach (Client item in clients)
            {
                if (item.id.ToString() == cblistaclientes.SelectedValue.ToString())
                {
                    txtSCedula.Text = item.cedula;
                    txtSnombre_comercial.Text = item.nombre_comercial;
                }
            }
            txtSnombre_comercial.Enabled = false;
            txtSCedula.Enabled = false;
        }

        private void cblistaclientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_dato_client();
        }


        public bool chack_datos()
        {
            if (txtMonto.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        // Watch that have correcions 
        public bool Check_edit()
        {
            if (txtMonto.Text == pay.monto
                && pay.cliente_id.ToString() == cblistaclientes.SelectedValue.ToString()
                && pay.mes == cbmes.SelectedItem.ToString()
                && pay.ano == dtano.Value.Year.ToString()
                && pay.nota == rtNota.Text)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        // exit the this form
        private void exit_form()
        {
            Form form = new Payment_indexQ();
            form.Show();
            this.Close();
        }

        //Save new pay in database
        private void Save_Parnet()
        {
            //Payment object with data
            Payment item = new Payment();
            item.cliente_id = Int32.Parse(cblistaclientes.SelectedValue.ToString()); ;
            item.mes = cbmes.SelectedItem.ToString();
            item.ano = dtano.Value.Year.ToString();
            item.monto = txtMonto.Text;
            item.nota = rtNota.Text;
            item.tipo = "Parcial";
            item.factura_electronica = "SI";
            item.estado = "Finalizado";
            item.active = 1;
            item.fecha = DateTime.Now.ToShortDateString();
            if (type_funcion == "edit")
            {
                try
                {
                    item.id = pay.id;
                    //SE LLAMA AL METODO DE GUARDAR
                    PayBOL.Modify_Payment(item);
                    MessageBox.Show("El pago de a guardado con exito");

                }
                catch (Exception)
                {

                    throw;
                }

            }
            else
            {
                try
                {
                    if (PayBOL.validar_pago(item.mes, item.ano, item.cliente_id.ToString(), item.tipo))
                    {
                        const string message = "Ya existe este pago, quieres realizar otro pago para este mes?";
                        const string caption = "Aviso";
                        var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            PayBOL.Save(item);
                            MessageBox.Show("El pago de a guardado con exito");
                        }
                    }
                    else
                    {
                        PayBOL.Save(item);
                        MessageBox.Show("El parcial de a guardado con exito");
                    }
                 }
                catch (Exception)
                {

                    throw;
                }
            }


        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (clients.Count != 0)
            {
                if (type_funcion == "edit")
                {
                    if (chack_datos())
                    {
                        MessageBox.Show("Todos los datos estan vacios");
                    }
                    else
                    {
                        if (Check_edit())
                        {
                            MessageBox.Show("No has realizado ningun cambio");
                        }
                        else
                        {
                            //Message
                            const string message = "Deseas realizar los cambios?";
                            const string caption = "Aviso";
                            var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    Save_Parnet();
                                    exit_form();
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (chack_datos())
                    {
                        MessageBox.Show("Todos los datos estan vacios");
                    }
                    else
                    {
                        //check the empty data
                        if (txtMonto.Text == "")
                        {
                            //Message
                            const string message = "Deseas guardar este socios con algunos datos vacios?";
                            const string caption = "Datos Vacios";
                            var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    Save_Parnet();
                                    exit_form();
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                Save_Parnet();
                                exit_form();
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No hay clientes para agregar pagos");
            }
                
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            exit_form();
        }

        private void txtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
    }
}
