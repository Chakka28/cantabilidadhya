﻿using Npgsql;
using Proyecto.DAL;
using Proyecto.Interfaces.Clients;
using Proyecto.Interfaces.Parnets;
using Proyecto.Interfaces.PaymentQuarterly;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces
{
    public partial class Main : Form
    {
        Conexion cdal = new Conexion();
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Cierra la aplicacion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();

        }

        /// <summary>
        /// Llama al index de Socios
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSocio_Click(object sender, EventArgs e)
        {
            
            Form formParnet = new Parnet_index();
            formParnet.Show();

        }

        /// <summary>
        /// Llama al index de clientes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnClient_Click_1(object sender, EventArgs e)
        {
            Form formclient = new Client_index();
            formclient.Show();
        }

        /// <summary>
        /// Llama al index de pagos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click_1(object sender, EventArgs e)
        {
            Form formPayment = new Payment_index();
            formPayment.Show();
        }

        /// <summary>
        /// Llama al index de parciales
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOtros_Pagos_Click(object sender, EventArgs e)
        {
            Form formPayment = new Payment_indexQ();
            formPayment.Show();
        }

        private void btnRespaldo_Click(object sender, EventArgs e)
        {
            try
            {
                cdal.resguardoBD();
                const string message = "Respaldo realizado ve a la dirección: \n C:/Registro base de datos";
                const string caption = "Aviso";
                var result = MessageBox.Show(message, caption,
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
