﻿using Proyecto.BOL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.Payments
{
    public partial class FormPayment : Form
    {
        private ClientBOL clienteBOL; // Clase de cliente BOL
        private string type_funcion; // Tipo de funcion que ejecutara eel form
        private List<Client> clients = new List<Client>();//Lista de clientes 
        public Payment pay = new Payment(); //Objeto de pagos
        private PaymentBOL PayBOL;// Clase de pagos BOL

        public FormPayment(string type, Payment payItem)
        {
            InitializeComponent();
            type_funcion = type;
            pay = payItem;
        }

        private void FormPayment_Load(object sender, EventArgs e)
        {
            clienteBOL = new ClientBOL();
            PayBOL = new PaymentBOL();
            envent_mains();
            Load_client();
            if (type_funcion == "edit")
            {
                edit();
            }
            if (type_funcion == "detail")
            {
                detail();
            }

            

        }

        /// <summary>
        /// Metodo que llena los campos para el detalle del pago
        /// </summary>
        public void detail()
        {
            cblistaclientes.SelectedValue = pay.cliente_id;
            cblistaclientes.Enabled = false;
            cbmes.SelectedItem = pay.mes;
            cbmes.Enabled = false;
            DateTime enteredDate = DateTime.Parse("01/01/" + pay.ano);
            dtano.Value = enteredDate;
            dtano.Enabled = false;
            cbfe.SelectedItem = pay.factura_electronica;
            cbfe.Enabled = false;
            txtMonto.Text = pay.monto;
            txtMonto.Enabled = false;
            cbEstado.SelectedItem = pay.estado;
            cbEstado.Enabled = false;
            rtNota.Text = pay.nota;
            rtNota.Enabled = false;
            cbTipo.SelectedItem = pay.tipo;
            cbTipo.Enabled = false;
            btnGuardar.Enabled = false;
        }

        /// <summary>
        /// Metodo que rellena los datos para editar pago
        /// </summary>
        public void edit()
        {
            cblistaclientes.SelectedValue = pay.cliente_id;
            cbmes.SelectedItem = pay.mes;
            DateTime enteredDate = DateTime.Parse("01/01/" + pay.ano);
            dtano.Value = enteredDate;
            cbfe.SelectedItem = pay.factura_electronica;
            txtMonto.Text = pay.monto;
            cbEstado.SelectedItem = pay.estado;
            cbTipo.SelectedItem = pay.tipo;
            rtNota.Text = pay.nota;
        }

        /// <summary>
        /// Acciones que hay que realizar al inicar el formulario
        /// </summary>
        public void envent_mains()
        {
            dtano.Format = DateTimePickerFormat.Custom;
            dtano.CustomFormat = "yyyy";
            dtano.ShowUpDown = true;
            cbmes.SelectedIndex = 0;
            cbfe.SelectedIndex = 0;
            cbTipo.SelectedIndex = 0;
            cbEstado.SelectedIndex = 0;
        }

        /// <summary>
        /// Boton de volver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVolver_Click_1(object sender, EventArgs e)
        {
            Form formMain = new Payment_index();
            this.Close();
            formMain.Show();
        }

        /// <summary>
        /// Carga los clientes
        /// </summary>
        public void Load_client()
        {
            clients = clienteBOL.Load_clients("");
            cblistaclientes.DataSource = clients;
            Load_dato_client();
        }

        /// <summary>
        /// Carga los datos de los clientes
        /// </summary>
        public void Load_dato_client()
        {
            foreach (Client item in clients)
            {
                if (item.id.ToString() == cblistaclientes.SelectedValue.ToString())
                {
                    txtSCedula.Text = item.cedula;
                    txtSnombre_comercial.Text = item.nombre_comercial;
                }
            }
            txtSnombre_comercial.Enabled = false;
            txtSCedula.Enabled = false;
        }

        private void cblistaclientes_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_dato_client();
        }

        /// <summary>
        /// Revisa el monto
        /// </summary>
        /// <returns></returns>
        public bool chack_datos()
        {
            if (txtMonto.Text == "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Revisa los datos al editar 
        /// </summary>
        /// <returns></returns>
        public bool Check_edit()
        {
            if (txtMonto.Text == pay.monto
                && pay.cliente_id.ToString() == cblistaclientes.SelectedValue.ToString()
                && pay.mes == cbmes.SelectedItem.ToString()
                && pay.ano == dtano.Value.Year.ToString()
                && pay.factura_electronica == cbfe.SelectedItem.ToString()
                && pay.estado == cbEstado.SelectedItem.ToString()
                && pay.tipo == cbTipo.SelectedItem.ToString()
                && pay.nota == rtNota.Text)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Guarda el pago
        /// </summary>
        private void Save_Parnet()
        {
            //Payment object with data
            Payment item = new Payment();
            item.cliente_id = Int32.Parse(cblistaclientes.SelectedValue.ToString()); ;
            item.mes = cbmes.SelectedItem.ToString();
            item.ano = dtano.Value.Year.ToString();
            item.factura_electronica = cbfe.SelectedItem.ToString();
            item.monto = txtMonto.Text;
            item.nota = rtNota.Text;
            item.tipo = cbTipo.SelectedItem.ToString();
            item.estado = cbEstado.SelectedItem.ToString();
            item.active = 1;
            item.fecha = DateTime.Now.ToShortDateString();
            if (type_funcion == "edit")
            {
                try
                {
                    item.id = pay.id;
                    //SE LLAMA AL METODO DE GUARDAR
                    PayBOL.Modify_Payment(item);
                    MessageBox.Show("El pago de a guardado con exito");

                }
                catch (Exception)
                {

                    throw;
                }

            }
            else
            {
                try
                {
                    if (PayBOL.validar_pago(item.mes, item.ano, item.cliente_id.ToString(), item.tipo))
                    {
                        const string message = "Ya existe este pago, quieres realizar otro pago para este mes?";
                        const string caption = "Aviso";
                        var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.YesNo,
                                         MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            PayBOL.Save(item);
                            MessageBox.Show("El pago de a guardado con exito");
                        }
                        else
                        {
                            MessageBox.Show("No se agrego pago");
                        }
                    }
                    else
                    {
                        PayBOL.Save(item);
                        MessageBox.Show("El pago de a guardado con exito");
                    }
                    

                }
                catch (Exception)
                {

                    throw;
                }
            }


        }

        // exit the this form
        private void exit_form()
        {
            Form form = new Payment_index();
            form.Show();
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(clients.Count != 0)
            {
                if (type_funcion == "edit")
                {
                    if (chack_datos())
                    {
                        MessageBox.Show("Todos los datos estan vacios");
                    }
                    else
                    {
                        if (Check_edit())
                        {
                            MessageBox.Show("No has realizado ningun cambio");
                        }
                        else
                        {
                            //Message
                            const string message = "Deseas realizar los cambios?";
                            const string caption = "Aviso";
                            var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    Save_Parnet();
                                    exit_form();
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (chack_datos())
                    {
                        MessageBox.Show("Todos los datos estan vacios");
                    }
                    else
                    {
                        //check the empty data
                        if (txtMonto.Text == "")
                        {
                            //Message
                            const string message = "Deseas guardar este socios con algunos datos vacios?";
                            const string caption = "Datos Vacios";
                            var result = MessageBox.Show(message, caption,
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question);
                            if (result == DialogResult.Yes)
                            {
                                try
                                {
                                    Save_Parnet();
                                    exit_form();
                                }
                                catch (Exception ex)
                                {

                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                        else
                        {
                            try
                            {
                                Save_Parnet();
                                exit_form();
                            }
                            catch (Exception ex)
                            {

                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No hay clientes para agregar pagos");
            }
            
        }

        /// <summary>
        /// Hace que solo se pueda usar numeros en el txtmonto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMonto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Para obligar a que sólo se introduzcan números
            if (Char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
              if (Char.IsControl(e.KeyChar)) //permitir teclas de control como retroceso
            {
                e.Handled = false;
            }
            else
            {
                //el resto de teclas pulsadas se desactivan
                e.Handled = true;
            }
        }
    }
}
