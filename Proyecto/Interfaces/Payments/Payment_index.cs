﻿using Proyecto.BOL;
using Proyecto.Entities;
using Proyecto.Interfaces.Payments;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces
{
    public partial class Payment_index : Form
    {
        PaymentBOL payBOL;//Clase de payment BOL
        private List<Client> clients = new List<Client>();//Lista de clientes
        private ClientBOL clienteBOL;//Clase de cliente BOL

        public Payment_index()
        {
            InitializeComponent();
        }

        private void Payment_index_Load(object sender, EventArgs e)
        {
            clienteBOL = new ClientBOL();
            payBOL = new PaymentBOL();
            Load_DataList();
            Load_client();
            dtano.Format = DateTimePickerFormat.Custom;
            dtano.CustomFormat = "yyyy";
            dtano.ShowUpDown = true;
        }

        /// <summary>
        /// Carga los clientes
        /// </summary>
        public void Load_client()
        {
            Client cl = new Client(){id = 0,nombre_cliente = ""};
            clients = clienteBOL.Load_clients("");
            clients.Insert(0, cl);
            cbClient.DataSource = clients;
        }

        /// <summary>
        /// Carga los pagos
        /// </summary>
        public void Load_DataList()
        {
            DGPagos.DataSource = payBOL.Load_Payment("","normal");
        }

        /// <summary>
        /// Cerrar form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnVolver_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Boton de agregar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Form_pay("");
        }

        /// <summary>
        /// Boton de detalle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDetalle_Click(object sender, EventArgs e)
        {
            Form_pay("detail");
        }

        /// <summary>
        /// Boton de eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, EventArgs e)
        {
            Form_pay("edit");
        }

        /// <summary>
        /// Metodo para llamar el fomulario 
        /// </summary>
        /// <param name="type"></param>
        private void Form_pay(string type)
        {
            if (type != "")
            {
                int row = DGPagos.SelectedRows.Count > 0 ? DGPagos.SelectedRows[0].Index : -1;
                if (row >= 0)
                {
                    Form formParnet = new FormPayment(type, (Payment)DGPagos.CurrentRow.DataBoundItem);
                    formParnet.Show();
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Seleccione un socio de la tabla");
                }
            }
            else
            {
                Payment mp = new Payment();
                Form formParnet = new FormPayment(type, mp);
                formParnet.Show();
                this.Close();
            }
        }

        /// <summary>
        /// Boton de eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            int row = DGPagos.SelectedRows.Count > 0 ? DGPagos.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                const string message = "Deseas eliminar este pago?";
                const string caption = "Eliminar pago";
                var result = MessageBox.Show(message, caption,
                                 MessageBoxButtons.YesNo,
                                 MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    payBOL.Delete_client((Payment)DGPagos.CurrentRow.DataBoundItem);
                    Load_DataList();
                }
            }
            else
            {
                MessageBox.Show("Seleccione un pago de la tabla");
            }
        }

        /// <summary>
        /// Limpiar los filtros
        /// </summary>
        public void limpiar_filtro()
        {
            DateTime enteredDate = DateTime.Parse(DateTime.Now.ToString());
            dtfecha.Value = enteredDate;
            cbClient.SelectedIndex = 0;
            cbmes.SelectedIndex = -1;
            dtano.Value = enteredDate;
            cbfe.SelectedIndex = -1;
            cbEstado.SelectedIndex = -1;
            Load_DataList();
        }

        /// <summary>
        /// Crea el filtro
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFiltro_Click(object sender, EventArgs e)
        {
            string filtro1 = "";
            string filtro2 = "";
            string filtro3 = "";
            string filtro4 = "";
            string filtro5 = "";
            string filtro6 = "";
            if (dtfecha.Checked == true)
            {
                filtro1 = dtfecha.Value.ToString("d/MM/yyyy");
            }
            if(cbClient.SelectedIndex != 0)
            {
                filtro2 = cbClient.SelectedValue.ToString();
            }
            if (cbmes.SelectedIndex != -1)
            {
                filtro3 = cbmes.SelectedItem.ToString();
            }
            if (dtano.Checked == true)
            {
                filtro4 = dtano.Value.Year.ToString();
            }
            if (cbfe.SelectedIndex != -1)
            {
                filtro5 = cbfe.SelectedItem.ToString();
            }
            if (cbEstado.SelectedIndex != -1)
            {
                filtro6 = cbEstado.SelectedItem.ToString();
            }

            DGPagos.DataSource = payBOL.Load_Payment_filter(filtro1, filtro2, filtro3, filtro4, filtro5, filtro6);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            limpiar_filtro();
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {
            Form formParnet = new Report(payBOL.Load_Payment("", "normal"), clienteBOL.Load_clients(""));
            formParnet.Show();
        }
    }
}
