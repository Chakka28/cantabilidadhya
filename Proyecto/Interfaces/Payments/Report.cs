﻿using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto.Interfaces.Payments
{
    public partial class Report : Form
    {
        List<Payment> pagos = new List<Payment>();//Lista de pagos
        List<Client> clientes = new List<Client>();//Lista de pagos

        public Report(List<Payment> item, List<Client> itemC)
        {
            InitializeComponent();
            armar_GridView();
            pagos = item;
            clientes = itemC;
            Cargar_GridView();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        /// <summary>
        /// Arma el data gridView
        /// </summary>
        private void armar_GridView(){
            DGReporte.Columns.Add("cliente", "Cliente");
            DGReporte.Columns.Add("cedula", "Cedula");
            if (DateTime.Now.Month == 1)
            {
                
                DGReporte.Columns.Add("mes_actual", mes(DateTime.Now.Month) + " " + DateTime.Now.Year.ToString());
                DGReporte.Columns.Add("liq.Anual", "Liq.Anual" + " " + DateTime.Now.Year.ToString());
            }
            else
            {
                DGReporte.Columns.Add("mes_actual", mes(DateTime.Now.Month) + " " + DateTime.Now.Year.ToString());
                DGReporte.Columns.Add("mes_anterior", mes(DateTime.Now.Month - 1) + " " + DateTime.Now.Year.ToString());
                DGReporte.Columns.Add("liq.Anual", "Liq.Anual" + " " + DateTime.Now.Year.ToString());
            }
            
        }

        /// <summary>
        /// Carga el data GridView
        /// </summary>
        private void Cargar_GridView()
        {
            foreach (Client item in clientes)
            {
                if (Cargar_GridView1(item.id, mes(DateTime.Now.Month)))
                {
                    if(Cargar_GridView1(item.id, mes(DateTime.Now.Month - 1)))
                    {
                        DGReporte.Rows.Add(item.nombre_cliente, item.cedula, "PAGO", "PAGO", Cargar_Liq_Anual(item.id));
                    }
                    else
                    {
                        DGReporte.Rows.Add(item.nombre_cliente, item.cedula, "PAGO", "SIN COBRAR", Cargar_Liq_Anual(item.id));
                    }

                }
                else
                {
                    if (Cargar_GridView1(item.id, mes(DateTime.Now.Month - 1)))
                    {
                        DGReporte.Rows.Add(item.nombre_cliente, item.cedula, "SIN COBRAR", "PAGO", Cargar_Liq_Anual(item.id));
                    }
                    else
                    {
                        DGReporte.Rows.Add(item.nombre_cliente, item.cedula, "SIN COBRAR", "SIN COBRAR", Cargar_Liq_Anual(item.id));
                    }
                }
            }
        }

        private bool Cargar_GridView1(int id, string mes)
        {
            
            foreach (Payment item2 in pagos)
            {
                if (item2.cliente_id == id)
                {
                    if (item2.mes == mes && item2.ano == DateTime.Now.Year.ToString() && item2.tipo == "Normal")
                    {
                        return true;
                    }
                 }
            }
            return false;
        }

        private string Cargar_Liq_Anual(int id)
        {
            foreach (Payment item2 in pagos)
            {
                if (item2.cliente_id == id && item2.tipo == "Liq.Anual" && item2.ano == DateTime.Now.Year.ToString())
                {
                    return "PAGO";
                }
            }
            return "SIN COBRAR";
        }

        /// <summary>
        /// String del mes
        /// </summary>
        /// <param name="m">Index del mes</param>
        /// <returns></returns>
        private string mes(int m)
        {
            if (m == 1)
            {
                return "Enero";
            }
            if (m == 2)
            {
                return "Febrero";
            }
            if (m == 3)
            {
                return "Marzo";
            }
            if (m == 4)
            {
                return "Abril";
            }
            if (m == 5)
            {
                return "Mayo";
            }
            if (m == 6)
            {
                return "Junio";
            }
            if (m == 7)
            {
                return "Julio";
            }
            if (m == 8)
            {
                return "Agosto";
            }
            if (m == 9)
            {
                return "Septiembre";
            }
            if (m == 10)
            {
                return "Octubre";
            }
            if (m == 11)
            {
                return "Noviembre";
            }
            else{
                return "Diciembre";
            }
        }

        private void Report_Load(object sender, EventArgs e)
        {

        }
    }
}
