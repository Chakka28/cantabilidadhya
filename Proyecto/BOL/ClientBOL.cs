﻿using Proyecto.DAL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.BOL
{
    class ClientBOL
    {
        // ParnetDAL class
        private ClientDAL clientDAL;
        private ParnetDAL pDAL;
        public ClientBOL()
        {
            clientDAL = new ClientDAL();
            pDAL = new ParnetDAL();
        }

        //Save Client
        public void Save(Client client)
        {
            clientDAL.Insert(client);
        }
        // Reload a list of Client
        internal List<Client> Load_clients(string filter)
        {
            return clientDAL.Load_client(filter);
        }
        // Delete Client
        internal void Delete_client(Client client)
        {
            clientDAL.Delete(client);
            clientDAL.Delete_pagos_cliente(client);
        }

        // Modify Client
        internal void Modify_parent(Client client)
        {
            clientDAL.Modify(client);
        }

        /// <summary>
        /// Ver si un cliente no esta eliminado
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal bool exists(string id)
        {
            Parnet p = new Parnet();
            p = pDAL.select_parnet(id);

            if(p.active == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
