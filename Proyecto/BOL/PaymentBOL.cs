﻿using Proyecto.DAL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.BOL
{
    class PaymentBOL
    {
        // PaymentDAL class
        private PaymentDAL payDAL;

        public PaymentBOL()
        {
            payDAL = new PaymentDAL();
        }

        //Save payment
        public void Save(Payment item)
        {
            payDAL.Insert(item);
        }
        // Reload a list of Client
        internal List<Payment> Load_Payment(string filter, string type)
        {
            return payDAL.Load_pay(filter, type);
        }
        // Reload a list of Client
        internal List<Payment> Load_Payment_parcial(string filter, string type)
        {
            return payDAL.Load_pay_Parcial(filter, type);
        }
        // Modify Payment
        internal void Modify_Payment(Payment item)
        {
            payDAL.Modify(item);
        }
        // delete Payment
        internal void Delete_client(Payment item)
        {
            payDAL.Delete(item);
        }
        // Reload a list of payment with filter
        internal List<Payment> Load_Payment_filter(string filter1, string filter2, string filter3, string filter4, string filter5, string filter6)
        {
            return payDAL.Load_pay_filter(filter1, filter2, filter3, filter4, filter5, filter6);
        }

        internal List<Payment> Load_Payment_filter_parcial(string filter1, string filter2, string filter3, string filter4)
        {
            return payDAL.Load_pay_filter_parcial(filter1, filter2, filter3, filter4);
        }

        internal bool validar_pago(string mes, string ano, string id_cliente, string tipo)
        {
            return payDAL.validarPago(mes, ano, id_cliente, tipo);
        }
    }
}
