﻿using Proyecto.DAL;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.BOL
{
    class ParnetBOL
    {
        // ParnetDAL class
        private ParnetDAL parnetDAL;

        public ParnetBOL()
        {
            parnetDAL = new ParnetDAL();
        }

        //Save parnet
        public void Save(Parnet parnet)
        {
            parnetDAL.Insert(parnet);
        }

        // Reload a list of partners
        internal List<Parnet> Load_parnets(string filter)
        {
            return parnetDAL.Load_Parnets(filter);
        }

        // Delete parent 
        internal void Delete_parent(Parnet parnet)
        {
            parnetDAL.Delete(parnet);
        }

        // Modify parent 
        internal void Modify_parent(Parnet parnet)
        {
            parnetDAL.Modify(parnet);
        }

    }
}
