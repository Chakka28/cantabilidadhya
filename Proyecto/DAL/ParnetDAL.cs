﻿using Npgsql;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.DAL
{
    class ParnetDAL
    {
        /// <summary>
        /// Agrega un nuevo socio a la base de datos
        /// </summary>
        /// <param name="parnet">Objeto del socio con los datos</param>
        internal void Insert(Parnet parnet)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                try
                {
                    con.Open();
                    string sql = "INSERT INTO public.parnet(nombre_socio, nombre_comercial, cedula, direccion, correo, clave_correo, " +
                        "fecha_nacimiento, celular, nice, ven, activo)" +
                        " VALUES(@ns, @nc, @ced, @dir, @cor, @cc, @fn, @cel, @nice, @ven, @act)";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@ns", parnet.nombre_socio);
                    cmd.Parameters.AddWithValue("@nc", parnet.nombre_comercial);
                    cmd.Parameters.AddWithValue("@ced", parnet.cedula);
                    cmd.Parameters.AddWithValue("@dir", parnet.direccion);
                    cmd.Parameters.AddWithValue("@cor", parnet.correo);
                    cmd.Parameters.AddWithValue("@cc", parnet.clave_correo);
                    cmd.Parameters.AddWithValue("@fn", parnet.nacimiento);
                    cmd.Parameters.AddWithValue("@cel", parnet.celular);
                    cmd.Parameters.AddWithValue("@nice", parnet.nise);
                    cmd.Parameters.AddWithValue("@ven", parnet.ven);
                    cmd.Parameters.AddWithValue("@act", 1);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                
            }
        }

        /// <summary>
        /// Modifica un socio de la base de datos
        /// </summary>
        /// <param name="parnet">Objeto del socio con los datos</param>
        public void Modify(Parnet parnet)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE public.parnet" +
                    " set nombre_socio =@ns" +
                    " ,nombre_comercial =@nc" +
                    " ,cedula =@ced" +
                    " ,direccion =@dir" +
                    " ,correo =@cor" +
                    " ,clave_correo =@cc" +
                    " ,fecha_nacimiento =@fn" +
                    " ,celular =@cel" +
                    " ,nice =@nise" +
                    " ,ven =@ven" +
                    " WHERE id_parnet =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ns", parnet.nombre_socio);
                cmd.Parameters.AddWithValue("@nc", parnet.nombre_comercial);
                cmd.Parameters.AddWithValue("@ced", parnet.cedula);
                cmd.Parameters.AddWithValue("@dir", parnet.direccion);
                cmd.Parameters.AddWithValue("@cor", parnet.correo);
                cmd.Parameters.AddWithValue("@cc", parnet.clave_correo);
                cmd.Parameters.AddWithValue("@fn", parnet.nacimiento);
                cmd.Parameters.AddWithValue("@cel", parnet.celular);
                cmd.Parameters.AddWithValue("@nise", parnet.nise);
                cmd.Parameters.AddWithValue("@ven", parnet.ven);
                cmd.Parameters.AddWithValue("@id", parnet.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }

        /// <summary>
        /// Elimina un socio de la base de datos
        /// </summary>
        /// <param name="parnet">Socio a eliminar</param>
        public void Delete(Parnet parnet)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE public.parnet " +
                    " set activo =@act" +
                    " WHERE id_parnet =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@act", 2);
                cmd.Parameters.AddWithValue("@id", parnet.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }


        /// <summary>
        /// Carga la lista de socios
        /// </summary>
        /// <param name="filter">En caso de busca por filtro</param>
        /// <returns> Un list de socios</returns>
        internal List<Parnet> Load_Parnets(string filter)
        {
            List<Parnet> parnets = new List<Parnet>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                string sql = "SELECT id_parnet, nombre_socio, nombre_comercial, cedula, direccion, " +
                    "correo, clave_correo, fecha_nacimiento, celular, nice, ven, activo " +
                    "FROM public.parnet WHERE activo = 1 ;";

                if (!String.IsNullOrEmpty(filter))
                {
                    sql = "SELECT id_parnet, nombre_socio, nombre_comercial, cedula, direccion, " +
                            "correo, clave_correo, fecha_nacimiento, celular, nice, ven, activo " +
                            "FROM public.parnet " +
                            "WHERE activo = 1 " +
                            "and lower(nombre_socio) like lower(@par) " +
                            "OR lower(nombre_comercial) like lower(@par) " +
                            "OR lower(correo) like lower(@par) " +
                            "OR lower(cedula) like lower(@par) ";
                }

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filter))
                {
                    cmd.Parameters.AddWithValue("@par", filter + "%");
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    parnets.Add(Laod_parnet(reader));
                }
                con.Close();
            }

            return parnets;
        }

        /// <summary>
        /// Crear socio
        /// </summary>
        /// <param name="reader">Lectura de la base de datos</param>
        /// <returns>objeto parent</returns>
        private Parnet Laod_parnet(NpgsqlDataReader reader)
        {
            Parnet parnet = new Parnet
            {
                id = reader.GetInt32(0),
                nombre_socio = reader.GetString(1),
                nombre_comercial = reader.GetString(2),
                cedula = reader.GetString(3),
                direccion = reader.GetString(4),
                correo = reader.GetString(5),
                clave_correo = reader.GetString(6),
                nacimiento = reader.GetString(7),
                celular = reader.GetString(8),
                nise = reader.GetString(9),
                ven = reader.GetString(10),
                active = reader.GetInt32(11)
            };

            return parnet;
        }

        /// <summary>
        /// Selecciona socio por id
        /// </summary>
        /// <param name="id">id del socio</param>
        /// <returns>parnet</returns>
        internal Parnet select_parnet(string id)
        {
            Parnet parnet = new Parnet();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                string sql = "SELECT id_parnet, nombre_socio, nombre_comercial, cedula, direccion, " +
                    "correo, clave_correo, fecha_nacimiento, celular, nice, ven, activo " +
                    "FROM public.parnet WHERE id_parnet = @id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", Int32.Parse(id));
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    parnet = Laod_parnet(reader);
                }
                con.Close();
            }

            return parnet;
        }

    }
}
