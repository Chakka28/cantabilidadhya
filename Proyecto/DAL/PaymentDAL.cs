﻿using Npgsql;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.DAL
{
    class PaymentDAL
    {

        internal void Insert(Payment pay)
        {
            // insert Payment in database
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                try
                {
                    con.Open();
                    string sql = "INSERT INTO public.pagos(id_cliente, fecha_pago, mes_pago, ano_pago," +
                        " monto, factura_electronica, nota, estado, tipo, activo)" +
                        " VALUES(@idc, @fp, @mp, @ap, @mon, @fe, @not, @est, @tip, @act)";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@idc", pay.cliente_id);
                    cmd.Parameters.AddWithValue("@fp", pay.fecha);
                    cmd.Parameters.AddWithValue("@mp", pay.mes);
                    cmd.Parameters.AddWithValue("@ap", pay.ano);
                    cmd.Parameters.AddWithValue("@mon", pay.monto);
                    cmd.Parameters.AddWithValue("@fe", pay.factura_electronica);
                    cmd.Parameters.AddWithValue("@not", pay.nota);
                    cmd.Parameters.AddWithValue("@est", pay.estado);
                    cmd.Parameters.AddWithValue("@tip", pay.tipo);
                    cmd.Parameters.AddWithValue("@act", 1);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }

            }
        }

        // delete payment
        public void Delete(Payment pay)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE public.pagos " +
                    " set activo =@act" +
                    " WHERE id_pagos =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@act", 2);
                cmd.Parameters.AddWithValue("@id", pay.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }

        // Modify Payment
        public void Modify(Payment pay)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE public.pagos" +
                    " set id_cliente =@idc" +
                    " ,fecha_pago =@fp" +
                    " ,mes_pago =@mp" +
                    " ,ano_pago =@ap" +
                    " ,monto =@mon" +
                    " ,factura_electronica =@fe" +
                    " ,nota =@not" +
                    " ,estado =@est" +
                    " ,tipo =@tip" +
                    " WHERE id_pagos =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idc", pay.cliente_id);
                cmd.Parameters.AddWithValue("@fp", pay.fecha);
                cmd.Parameters.AddWithValue("@mp", pay.mes);
                cmd.Parameters.AddWithValue("@ap", pay.ano);
                cmd.Parameters.AddWithValue("@mon", pay.monto);
                cmd.Parameters.AddWithValue("@fe", pay.factura_electronica);
                cmd.Parameters.AddWithValue("@not", pay.nota);
                cmd.Parameters.AddWithValue("@est", pay.estado);
                cmd.Parameters.AddWithValue("@tip", pay.tipo);
                cmd.Parameters.AddWithValue("@id", pay.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }

        // Load Payment
        private Payment Laod_payment(NpgsqlDataReader reader)
        {
            ClientDAL cdal = new ClientDAL();
            Payment pay = new Payment
            {
                id = reader.GetInt32(0),
                cliente_id = reader.GetInt32(1),
                fecha = reader.GetString(2),
                mes = reader.GetString(3),
                ano = reader.GetString(4),
                monto = reader.GetString(5),
                factura_electronica = reader.GetString(6),
                nota = reader.GetString(7),
                estado = reader.GetString(8),
                tipo = reader.GetString(9),
                active = reader.GetInt32(10)
            };
            pay.nombre_cliente = cdal.select_client(pay.cliente_id).nombre_cliente;

            return pay;
        }

        // load a list from parnets
        internal List<Payment> Load_pay(string filter, string type)
        {
            List<Payment> payment = new List<Payment>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                string sql = "SELECT id_pagos, id_cliente, fecha_pago, mes_pago, ano_pago, monto, " +
                        "factura_electronica, nota, estado, tipo, activo " +
                    "FROM public.pagos WHERE activo = 1 and tipo IN ('Normal','Liq.Anual');";
                con.Open();


                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    payment.Add(Laod_payment(reader));
                }
                con.Close();
            }

            return payment;
        }

        // load a list from parnets
        internal List<Payment> Load_pay_Parcial(string filter, string type)
        {
            List<Payment> payment = new List<Payment>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                string sql = "SELECT id_pagos, id_cliente, fecha_pago, mes_pago, ano_pago, monto, " +
                        "factura_electronica, nota, estado, tipo, activo " +
                    "FROM public.pagos WHERE activo = 1 and tipo = 'Parcial';";
                con.Open();


                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    payment.Add(Laod_payment(reader));
                }
                con.Close();
            }

            return payment;
        }


        // load a list from parnets
        internal List<Payment> Load_pay_filter(string filter1, string filter2, string filter3, string filter4, string filter5, string filter6)
        {
            List<Payment> payment = new List<Payment>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                string sql = "SELECT id_pagos, id_cliente, fecha_pago, mes_pago, ano_pago, monto, " +
                        "factura_electronica, nota, estado, tipo, activo " +
                        "FROM public.pagos WHERE activo = 1 and tipo IN ('Normal','Liq.Anual')";
                con.Open();
                if(filter1 != "")
                {
                    sql += " and fecha_pago = @fp";
                }
                if (filter2 != "")
                {
                    sql += " and id_cliente = @idc";
                }
                if (filter3 != "")
                {
                    sql += " and mes_pago = @mp";
                }
                if (filter4 != "")
                {
                    sql += " and ano_pago = @ap";
                }
                if (filter5 != "")
                {
                    sql += " and factura_electronica = @fe";
                }
                if (filter6 != "")
                {
                    sql += " and estado = @est";
                }


                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                Console.WriteLine(sql);


                if (filter1 != "")
                {
                    cmd.Parameters.AddWithValue("@fp", filter1);
                }
                if (filter2 != "")
                {
                    int m = Int32.Parse(filter2);
                    cmd.Parameters.AddWithValue("@idc", m);
                }
                if (filter3 != "")
                {
                    cmd.Parameters.AddWithValue("@mp", filter3);
                }
                if (filter4 != "")
                {
                    cmd.Parameters.AddWithValue("@ap", filter4);
                }
                if (filter5 != "")
                {
                    cmd.Parameters.AddWithValue("@fe", filter5);
                }
                if (filter6 != "")
                {
                    cmd.Parameters.AddWithValue("@est", filter6);
                }

                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    payment.Add(Laod_payment(reader));
                }
                con.Close();
            }

            return payment;
        }



        // load a list from parnets
        internal List<Payment> Load_pay_filter_parcial(string filter1, string filter2, string filter3, string filter4)
        {
            List<Payment> payment = new List<Payment>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                string sql = "SELECT id_pagos, id_cliente, fecha_pago, mes_pago, ano_pago, monto, " +
                        "factura_electronica, nota, estado, tipo, activo " +
                        "FROM public.pagos WHERE activo = 1 and tipo = 'Parcial'";
                con.Open();
                if (filter1 != "")
                {
                    sql += " and fecha_pago = @fp";
                }
                if (filter2 != "")
                {
                    sql += " and id_cliente = @idc";
                }
                if (filter3 != "")
                {
                    sql += " and mes_pago = @mp";
                }
                if (filter4 != "")
                {
                    sql += " and ano_pago = @ap";
                }


                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                Console.WriteLine(sql);


                if (filter1 != "")
                {
                    cmd.Parameters.AddWithValue("@fp", filter1);
                }
                if (filter2 != "")
                {
                    int m = Int32.Parse(filter2);
                    cmd.Parameters.AddWithValue("@idc", m);
                }
                if (filter3 != "")
                {
                    cmd.Parameters.AddWithValue("@mp", filter3);
                }
                if (filter4 != "")
                {
                    cmd.Parameters.AddWithValue("@ap", filter4);
                }

                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    payment.Add(Laod_payment(reader));
                }
                con.Close();
            }

            return payment;
        }

        internal bool validarPago(string mes, string ano, string id_cliente, string tipo)
        {
            Payment p = new Payment();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                string sql = "SELECT id_pagos, id_cliente, fecha_pago, mes_pago, ano_pago, monto, " +
                        "factura_electronica, nota, estado, tipo, activo " +
                        "FROM public.pagos WHERE activo = 1 and  mes_pago = @mp and ano_pago = @ap and id_cliente = @idc and tipo = @tp";
                con.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@mp", mes);
                cmd.Parameters.AddWithValue("@ap", ano);
                cmd.Parameters.AddWithValue("@idc", Int32.Parse(id_cliente));
                cmd.Parameters.AddWithValue("@tp", tipo);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    p = Laod_payment(reader);
                }
                con.Close();
                if (p.fecha != null)
                {
                    Console.WriteLine("ESTE ES EL ID: " + p.fecha);
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


    }
}
