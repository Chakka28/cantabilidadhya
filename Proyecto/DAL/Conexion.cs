﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Npgsql;

namespace Proyecto.DAL
{
    public class Conexion
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;

        internal void resguardoBD()
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                try
                {
                    con.Open();
                    string sql = "copy parnet(id_parnet, nombre_socio, nombre_comercial, cedula, direccion,correo, " +
                        "clave_correo, fecha_nacimiento, celular, nice, ven, activo) to " +
                        "'C:/Registro base de datos/socios.csv' delimiter ';'  CSV HEADER; " +
                        "copy client(id_cliente, id_socio, nombre_cliente, nombre_comercial, cedula, direccion, correo," +
                        "clave_correo, fecha_nacimiento, celular, nice,ven, activo) " +
                        "to 'C:/Registro base de datos/clientes.csv' delimiter ';'  CSV HEADER; " +
                        "copy pagos(id_pagos, id_cliente, fecha_pago, mes_pago, ano_pago, monto,factura_electronica, " +
                        "nota, estado, tipo, activo) " +
                        "to 'C:/Registro base de datos/pagos.csv' delimiter ';'  CSV HEADER; ";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception)
                {
                    throw new Exception("Error al hacer el resguardo de la base de datos");
                }

            }
        }
    }

    
}
