﻿using Npgsql;
using Proyecto.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.DAL
{
    class ClientDAL
    {
        /// <summary>
        /// Inserta un cliente a la base de datos
        /// </summary>
        /// <param name="item">cliente</param>
        internal void Insert(Client item)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                try
                {
                    con.Open();
                    string sql = "INSERT INTO public.client(id_socio, nombre_cliente, nombre_comercial, cedula, direccion, correo, " +
                        "clave_correo, fecha_nacimiento, celular, nice, ven, activo)" +
                        " VALUES(@ids, @ns, @nc, @ced, @dir, @cor, @cc, @fn, @cel, @nice, @ven, @act)";
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                    cmd.Parameters.AddWithValue("@ids",item.id_socio);
                    cmd.Parameters.AddWithValue("@ns", item.nombre_cliente);
                    cmd.Parameters.AddWithValue("@nc", item.nombre_comercial);
                    cmd.Parameters.AddWithValue("@ced", item.cedula);
                    cmd.Parameters.AddWithValue("@dir", item.direccion);
                    cmd.Parameters.AddWithValue("@cor", item.correo);
                    cmd.Parameters.AddWithValue("@cc", item.clave_correo);
                    cmd.Parameters.AddWithValue("@fn", item.nacimiento);
                    cmd.Parameters.AddWithValue("@cel", item.celular);
                    cmd.Parameters.AddWithValue("@nice", item.nise);
                    cmd.Parameters.AddWithValue("@ven", item.ven);
                    cmd.Parameters.AddWithValue("@act", 1);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {

                    throw new Exception(ex.Message);
                }

            }
        }

        /// <summary>
        /// Carga la lista de clientes
        /// </summary>
        /// <param name="filter">filtro para busquedas</param>
        /// <returns></returns>
        internal List<Client> Load_client(string filter)
        {
            List<Client> client = new List<Client>();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                string sql = "SELECT id_cliente, nombre_cliente, nombre_comercial, cedula, direccion, " +
                    "correo, clave_correo, fecha_nacimiento, celular, nice, ven, activo, id_socio " +
                    "FROM public.client WHERE activo = 1 ;";

                if (!String.IsNullOrEmpty(filter))
                {
                    sql = "SELECT id_cliente, nombre_cliente, nombre_comercial, cedula, direccion, " +
                            "correo, clave_correo, fecha_nacimiento, celular, nice, ven, activo, id_socio " +
                            "FROM public.client " +
                            "WHERE activo = 1 " +
                            "and lower(nombre_cliente) like lower(@par) " +
                            "OR lower(nombre_comercial) like lower(@par) " +
                            "OR lower(correo) like lower(@par) " +
                            "OR lower(cedula) like lower(@par) ";
                }

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                if (!String.IsNullOrEmpty(filter))
                {
                    cmd.Parameters.AddWithValue("@par", filter + "%");
                }
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {


                    client.Add(Laod_client(reader));
                }
                con.Close();
            }

            return client;
        }

        /// <summary>
        /// Cliente
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private Client Laod_client(NpgsqlDataReader reader)
        {
            Client client = new Client
            {
                id = reader.GetInt32(0),
                nombre_cliente = reader.GetString(1),
                nombre_comercial = reader.GetString(2),
                cedula = reader.GetString(3),
                direccion = reader.GetString(4),
                correo = reader.GetString(5),
                clave_correo = reader.GetString(6),
                nacimiento = reader.GetString(7),
                celular = reader.GetString(8),
                nise = reader.GetString(9),
                ven = reader.GetString(10),
                active = reader.GetInt32(11),
                id_socio = reader.GetInt32(12),
            };
            return client;
        }

        /// <summary>
        /// Elimina cliente de la base de datos
        /// </summary>
        /// <param name="client">cliente a eliminar</param>
        public void Delete(Client client)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE  public.client " +
                    " set activo =@act" +
                    " WHERE id_cliente =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@act", 2);
                cmd.Parameters.AddWithValue("@id", client.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }

        public void Delete_pagos_cliente(Client client)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE  public.pagos " +
                    " set activo =@act" +
                    " WHERE id_cliente =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@act", 2);
                cmd.Parameters.AddWithValue("@id", client.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }

        /// <summary>
        /// Modifica un cliente de la base de datos
        /// </summary>
        /// <param name="item">Cliente a modificar</param>
        public void Modify(Client item)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();
                string sql = "UPDATE public.client" +
                    " set nombre_cliente =@ns" +
                    " ,nombre_comercial =@nc" +
                    " ,cedula =@ced" +
                    " ,direccion =@dir" +
                    " ,correo =@cor" +
                    " ,clave_correo =@cc" +
                    " ,fecha_nacimiento =@fn" +
                    " ,celular =@cel" +
                    " ,nice =@nise" +
                    " ,ven =@ven" +
                    " ,id_socio =@ids" +
                    " WHERE id_cliente =@id ";

                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@ns", item.nombre_cliente);
                cmd.Parameters.AddWithValue("@nc", item.nombre_comercial);
                cmd.Parameters.AddWithValue("@ced", item.cedula);
                cmd.Parameters.AddWithValue("@dir", item.direccion);
                cmd.Parameters.AddWithValue("@cor", item.correo);
                cmd.Parameters.AddWithValue("@cc", item.clave_correo);
                cmd.Parameters.AddWithValue("@fn", item.nacimiento);
                cmd.Parameters.AddWithValue("@cel", item.celular);
                cmd.Parameters.AddWithValue("@nise", item.nise);
                cmd.Parameters.AddWithValue("@ven", item.ven);
                cmd.Parameters.AddWithValue("@ids", item.id_socio);
                cmd.Parameters.AddWithValue("@id", item.id);
                cmd.ExecuteNonQuery();

                con.Close();

            }
        }

        /// <summary>
        /// Carga cliente por id
        /// </summary>
        /// <param name="id"> id de cliente</param>
        /// <returns></returns>
        internal Client select_client(int id)
        {
            Client cliente = new Client();
            using (NpgsqlConnection con = new NpgsqlConnection(Conexion.conStr))
            {
                con.Open();

                string sql = "SELECT id_cliente, nombre_cliente, nombre_comercial, cedula, direccion, " +
                    "correo, clave_correo, fecha_nacimiento, celular, nice, ven, activo, id_socio " +
                    "FROM public.client WHERE id_cliente = @id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    cliente = Laod_client(reader);
                }
                con.Close();
            }

            return cliente;
        }

       
    }
}
